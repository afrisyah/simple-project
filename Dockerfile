FROM php:8.2-fpm-alpine
MAINTAINER afriansyah
RUN apk update && apk add bash nginx
COPY ./index.php /var/www/html/
COPY ./default.conf /etc/nginx/http.d/
EXPOSE 80
ENTRYPOINT ["/bin/bash", "-c", "(php-fpm &) && nginx -g 'daemon off;'"]
